package com.example.demoroomdatabase.model

import androidx.annotation.WorkerThread

class GuideRepository (private val guideDAO: GuideDAO) {

    val allGuides = guideDAO.getAlphabetizedGuides()
//    val allGuides = guideDAO.getAlphabetizedGuides()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(guide: Guide) {
        guideDAO.insertGuide(guide)
    }
}