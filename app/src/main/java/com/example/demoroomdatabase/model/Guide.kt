package com.example.demoroomdatabase.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "guide_table")
class Guide(
    @PrimaryKey @ColumnInfo(name = "tittle") val tittle: String,
    @ColumnInfo(name = "description") val description: String
) :Serializable{
}