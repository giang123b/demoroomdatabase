package com.example.demoroomdatabase.model

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope

@Database(entities = [Guide::class], version = 1)
abstract class GuideRoomDatabase : RoomDatabase() {

    abstract fun guideDAO(): GuideDAO

    companion object {
        @Volatile
        private var INSTANCE: GuideRoomDatabase? = null

        fun getDatabase(context: Context, applicationScope: CoroutineScope): GuideRoomDatabase {

            Log.e("GuideRoomDatabase", " getDatabase")

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    GuideRoomDatabase::class.java,
                    "guide_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

    }
}