package com.example.demoroomdatabase.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface GuideDAO {

    @Query("SELECT * FROM guide_table ORDER BY tittle ASC")
    fun getAlphabetizedGuides(): Flow<List<Guide>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertGuide(guide: Guide)

    @Query("DELETE FROM guide_table WHERE tittle = :tittle")
    suspend fun deleteGuide(tittle: String)

    @Query("UPDATE guide_table SET description=:description WHERE tittle = :tittle")
    fun updateGuide(description: String, tittle: String)

    @Query("SELECT * FROM guide_table WHERE tittle LIKE :search OR description LIKE :search")
    fun searchGuide(search: String): List<Guide>
}