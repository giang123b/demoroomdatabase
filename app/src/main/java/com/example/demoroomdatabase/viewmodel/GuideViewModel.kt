package com.example.demoroomdatabase

import androidx.lifecycle.*
import com.example.demoroomdatabase.model.Guide
import com.example.demoroomdatabase.model.GuideRepository
import kotlinx.coroutines.launch

class GuideViewModel (private val guideRepository: GuideRepository) : ViewModel() {

    val allWords: LiveData<List<Guide>> = guideRepository.allGuides.asLiveData()

    fun insert(guide: Guide) = viewModelScope.launch {
        guideRepository.insert(guide)
    }
}

class GuideViewModelFactory(private val guideRepository: GuideRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GuideViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return GuideViewModel(guideRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}