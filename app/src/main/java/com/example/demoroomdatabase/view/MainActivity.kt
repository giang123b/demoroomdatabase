package com.example.demoroomdatabase.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.activity.viewModels
import androidx.lifecycle.observe
import com.example.demoroomdatabase.*
import com.example.demoroomdatabase.model.Guide
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val newGuideActivityRequestCode = 1
    private val guideViewModel: GuideViewModel by viewModels {
        GuideViewModelFactory((application as GuidesApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = GuideListAdapter()
        recyclerviewGuide.adapter = adapter
        recyclerviewGuide.layoutManager = LinearLayoutManager(this)

        guideViewModel.allWords.observe(owner = this) { guides ->
            guides.let {
                adapter.setData(it)
                Log.e("MainActivity", it.toString())
            }
        }

        guideViewModel.insert(Guide("Viet Nam", "Mo ta"))
        guideViewModel.insert(Guide("Viet Nam1", "Mo ta"))
        guideViewModel.insert(Guide("Viet Nam3", "Mo ta"))

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewGuideActivity::class.java)
            startActivityForResult(intent, newGuideActivityRequestCode)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intentData: Intent?) {
        super.onActivityResult(requestCode, resultCode, intentData)

        if (requestCode == newGuideActivityRequestCode && resultCode == Activity.RESULT_OK) {
            intentData?.getSerializableExtra(NewGuideActivity.EXTRA_REPLY)?.let { reply ->
                val guide : Guide = reply as Guide
                guideViewModel.insert(guide)
            }
        } else {
            Toast.makeText(
                applicationContext,
                "Add new fail!",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
