package com.example.demoroomdatabase.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.demoroomdatabase.model.Guide
import com.example.demoroomdatabase.R
import com.example.demoroomdatabase.view.GuideListAdapter.GuideViewHolder

class GuideListAdapter : RecyclerView.Adapter<GuideViewHolder>() {

    private val guides = ArrayList<Guide>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuideViewHolder {
        return GuideViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: GuideViewHolder, position: Int) {
        val current: Guide = guides[position]
        holder.bind(current)
    }

    class GuideViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textViewTittle: TextView = itemView.findViewById(R.id.textViewTittle)
        private val textViewDescription: TextView = itemView.findViewById(R.id.textViewDescription)

        fun bind(guide: Guide) {
            textViewTittle.text = guide.tittle
            textViewDescription.text = guide.description
        }

        companion object {
            fun create(parent: ViewGroup): GuideViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_guide, parent, false)
                return GuideViewHolder(view)
            }
        }
    }

    fun setData(newGuides: List<Guide>) {
        val diffCallback = GuideDiffUtil(guides, newGuides)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        guides.clear()
        guides.addAll(newGuides)
        diffResult.dispatchUpdatesTo(this)
    }

    class GuideDiffUtil(private val oldList: List<Guide>, private val newList: List<Guide>) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].tittle == newList[newItemPosition].tittle
        }

    }


    override fun getItemCount(): Int = guides.size
}