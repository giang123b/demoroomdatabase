package com.example.demoroomdatabase.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import com.example.demoroomdatabase.model.Guide
import com.example.demoroomdatabase.R
import kotlinx.android.synthetic.main.activity_new_guide.*

class NewGuideActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_guide)

        buttonSave.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(editTextTittle.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val guide = Guide(editTextTittle.text.toString(), editTextDescription.text.toString())
                replyIntent.putExtra(EXTRA_REPLY, guide)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val EXTRA_REPLY = "ADDNEWGUIDE"
    }
}